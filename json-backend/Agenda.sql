BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `Agenda` (
	`Id`	TEXT NOT NULL,
	`Name`	TEXT NOT NULL,
	`Surname`	TEXT NOT NULL,
	`Telephone`	INTEGER NOT NULL,
	`Email`	TEXT NOT NULL,
	PRIMARY KEY(`Id`)
);
INSERT INTO `Agenda` VALUES ('uno','Amadeo','Garrido',665665668,'amadeo@gmail.com');
INSERT INTO `Agenda` VALUES ('dos','Amparo','Montero',265181181,'ahjashgo@gmail.com');
INSERT INTO `Agenda` VALUES ('tres','Elias','Lerrido',151161581,'lalelo@gmail.com');
INSERT INTO `Agenda` VALUES ('cuatro','Tobias','Willframe',515151565,'tobi@gmail.com');
COMMIT;
