import { shallowMount } from '@vue/test-utils'
import DeleteContact from '@/components/DeleteContact.vue'

test('Pintar botón back', () => {
    const wrapper = shallowMount(DeleteContact, {})
    let buttonBack = wrapper.find('.back-delete-button')
    expect(buttonBack.is('.back-delete-button')).toBe(true)
})

test('Pintar botón delete', () => {
    const wrapper = shallowMount(DeleteContact, {})
    let buttonDelete = wrapper.find('.delete-button')
    expect(buttonDelete.is('.delete-button')).toBe(true)
})

test('Ejecuta el botón back', async () => {
    const wrapper = shallowMount(DeleteContact, {
        propsData: {},
    })

    expect(wrapper.emitted().click).toBe(undefined)

    const button = wrapper.findAll('button').wrappers
    button[0].trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['button-back-event'].length).toBe(1)
    expect(wrapper.emitted()['button-back-event'][0]).toEqual([])
})

test('Ejecuta el botón delete', async () => {
    const wrapper = shallowMount(DeleteContact, {
        propsData: {},
    })

    expect(wrapper.emitted().click).toBe(undefined)

    const button = wrapper.findAll('button').wrappers
    button[1].trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['button-delete-event'].length).toBe(1)
    expect(wrapper.emitted()['button-delete-event'][0]).toEqual([])
})
