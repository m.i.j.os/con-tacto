import { shallowMount } from '@vue/test-utils'
import ContactItem from '@/components/ContactItem.vue'

test('Crear componente ContactItem.vue', () => {
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            msg: 'me ves?',
            simpleContact: { propType: null, required: null },
        },
    })
    expect(wrapper.text()).toContain('me ves?')
})

test('pinta el botón', () => {
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            msg: { proptype: String },
            simpleContact: { propType: null, required: null },
        },
    })
    let button = wrapper.find('button')
    expect(button.is('button')).toBe(true)
})

test('ejecuta el click', async () => {
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            simpleContact: {
                id: '1',
                name: 'María',
                surname: 'Fernández',
                telephone: '45564356',
                email: 'blabla@gmail.com',
            },
        },
    })
    expect(wrapper.emitted()['click-to-info']).toBe(undefined)
    const button = wrapper.findAll('button')
    button.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted()['click-to-info'].length).toBe(1)
    expect(wrapper.emitted()['click-to-info'][0]).toEqual([
        {
            id: '1',
            name: 'María',
            surname: 'Fernández',
            telephone: '45564356',
            email: 'blabla@gmail.com',
        },
    ])
})

test('Crear un Computed para refactorizar', async () => {
    const wrapper = shallowMount(ContactItem, {
        propsData: {
            simpleContact: {
                name: 'Imanol',
                surname: 'Garrido',
            },
        },
    })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.fullName).toContain('Imanol Garrido')
})
