import { shallowMount } from '@vue/test-utils'
import ContactList from '@/components/ContactList.vue'
import ContactItem from '@/components/ContactItem.vue'

test('Crear componente ContactList.vue', () => {
    const wrapper = shallowMount(ContactList, {
        propsData: {
            msg: 'Agenda',
            contacts: [{ id: 1, name: 'Joseba', surname: 'Montero' }],
        },
    })
    console.log('contactsssss', wrapper.contacts)

    expect(wrapper.text()).toContain('Agenda')
})

test('Pinta el input del formulario en pantalla', () => {
    const wrapper = shallowMount(ContactList, {
        propsData: {
            msg: 'Agenda',
            contacts: [{ id: 1, name: 'Joseba', surname: 'Montero' }],
        },
    })
    let input = wrapper.find('input')
    expect(input.is('input')).toBe(true)
    let button = wrapper.find('button')
    expect(button.is('button')).toBe(true)
})

test('Escucha el click del boton y activa el cambio de pantalla "add-screen"', async () => {
    const wrapper = shallowMount(ContactList, {
        propsData: {
            msg: null,
            contacts: [{ id: 1, name: 'Joseba', surname: 'Montero' }],
        },
    })
    console.log('contactsssss', wrapper.contacts)
    expect(wrapper.emitted()['activate-add-screen']).toBe(undefined)
    const button = wrapper.findAll('button')
    button.trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.emitted()['activate-add-screen'].length).toBe(1)
    expect(wrapper.emitted()['activate-add-screen'][0]).toEqual([])
})
