import { shallowMount } from '@vue/test-utils'
import App from '@/App.vue'
import ContactAddEdit from '@/components/ContactAddEdit.vue'
import ContactList from '@/components/ContactList.vue'
import ContactDetails from '@/components/ContactDetails.vue'

test('Cambia el estado (action)', () => {
    const wrapper = shallowMount(App, {
        data() {
            return {
                action: null,
                screen: 'addEdit',
            }
        },
    })

    const addEddit = wrapper.find(ContactAddEdit)

    expect(wrapper.vm.action).toBe(null)

    addEddit.vm.$emit('change-action-edit')

    expect(wrapper.vm.action).toBe('edit')
})

test('Funciona el Mounted y carga datos del json', async () => {
    const MockApiCall = jest.fn()
    const mockContacts = [
        {
            id: '1',
            name: 'María',
            familyName: 'Fernández',
            telephone: '4563245',
            email: 'mfer@gmail.com',
        },
        {
            id: '2',
            name: 'Yeray',
            familyName: 'Espinosa',
            telephone: '7435886',
            email: 'asunto.espinoso@gmail.com',
        },
    ]
    MockApiCall.mockReturnValue(mockContacts)
    const wrapper = shallowMount(App, {
        data() {
            return {
                agendaData: null,
            }
        },
        methods: {
            ...App.methods,
            loadAgenda: MockApiCall,
        },
    })
    await wrapper.vm.$nextTick()
    expect(MockApiCall).toHaveBeenCalled()
    expect(wrapper.vm.agendaData).toEqual(mockContacts)
})

test('Pasa la prop selectedContact vacía cuando el action es add', async () => {
    const wrapper = shallowMount(App, {
        data() {
            return {
                action: null,
                selectedContact: {
                    id: '1',
                    name: 'Jon',
                    surname: 'Oyanguren',
                    telephone: '234234234',
                    email: 'jonoyanguren@peñascal.com',
                },
            }
        },
    })

    const contactList = wrapper.find(ContactList)

    contactList.vm.$emit('activate-add-screen')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.selectedContact).toEqual({
        id: null,
        name: null,
        surname: null,
        telephone: null,
        email: null,
    })
})

test('Funciona el method add/edit contact ', async () => {
    const MockApiSendEditContact = jest.fn()
    const mockContact = [
        {
            id: '1',
            name: 'María',
            surname: 'Fernández',
            telephone: '46345',
            email: 'mf@gmail.com',
        },
    ]
    MockApiSendEditContact.mockReturnValue(mockContact)
    const wrapper = shallowMount(App, {
        data() {
            return {}
        },
        methods: {
            ...App.methods,
            acceptAdditionEdition: MockApiSendEditContact,
        },
    })
    const localContact = {
        id: '1',
        name: 'María',
        surname: 'Fernández',
        telephone: '46345',
        email: 'mf@gmail.com',
    }
    console.log(wrapper)
    wrapper.vm.screen = 'addEdit'
    await wrapper.vm.$nextTick()
    const editContact = wrapper.find(ContactAddEdit)
    editContact.vm.$emit('emit-ok', localContact)
    await wrapper.vm.$nextTick()
    expect(MockApiSendEditContact).toHaveBeenCalled()
})

test('Funciona el method delete contact ', async () => {
    const MockApiSendEditContact = jest.fn()
    const mockContact = [
        {
            id: '1',
            name: 'María',
            surname: 'Fernández',
            telephone: '46345',
            email: 'mf@gmail.com',
        },
    ]
    MockApiSendEditContact.mockReturnValue(mockContact)
    const wrapper = shallowMount(App, {
        data() {
            return {}
        },
        methods: {
            ...App.methods,
            deleteContactChangeScreens: MockApiSendEditContact,
        },
    })
    const selectedContact = {
        id: '1',
        name: 'María',
        surname: 'Fernández',
        telephone: '46345',
        email: 'mf@gmail.com',
    }
    console.log(wrapper)
    wrapper.vm.screen = 'contactDetails'
    await wrapper.vm.$nextTick()
    const contactDetails = wrapper.find(ContactDetails)
    contactDetails.vm.$emit('send-to-delete', selectedContact)
    await wrapper.vm.$nextTick()
    expect(MockApiSendEditContact).toHaveBeenCalled()
})
