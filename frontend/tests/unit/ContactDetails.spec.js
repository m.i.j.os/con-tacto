import { shallowMount } from '@vue/test-utils'
import ContactDetails from '@/components/ContactDetails.vue'
import DeleteContact from '@/components/DeleteContact.vue'

test('Test pintar botón back', () => {
    const wrapper = shallowMount(ContactDetails, {
        propsData: {
            selectedContact: {
                propType: null,
                required: true,
            },
        },
    })

    let buttonContactBack = wrapper.find('.button-back')
    expect(buttonContactBack.is('.button-back')).toBe(true)
})

test('Pintar botón delete', () => {
    const wrapper = shallowMount(ContactDetails, {
        propsData: {
            selectedContact: {
                propType: null,
                required: true,
            },
        },
    })

    let buttonContactDelete = wrapper.find('.button-delete')
    expect(buttonContactDelete.is('.button-delete')).toBe(true)
})

test('Pintar botón edit', () => {
    const wrapper = shallowMount(ContactDetails, {
        propsData: {
            selectedContact: {
                propType: null,
                required: true,
            },
        },
    })
    let buttonContactEdit = wrapper.find('.button-edit')
    expect(buttonContactEdit.is('.button-edit')).toBe(true)
})

test('Ejecuta el botón back', async () => {
    const wrapper = shallowMount(ContactDetails, {
        propsData: {
            selectedContact: {
                id: '1',
                name: 'María',
                surname: 'Fernández',
                telephone: '45564356',
                email: 'blabla@gmail.com',
            },
        },
    })

    expect(wrapper.emitted()['button-back']).toBe(undefined)

    const button = wrapper.findAll('button').wrappers
    button[0].trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['button-back'].length).toBe(1)
    expect(wrapper.emitted()['button-back'][0]).toEqual([])
})

test('Ejecuta el botón editar', async () => {
    const wrapper = shallowMount(ContactDetails, {
        propsData: {
            selectedContact: {
                id: '1',
                name: 'María',
                surname: 'Fernández',
                telephone: '45564356',
                email: 'blabla@gmail.com',
            },
        },
    })

    expect(wrapper.emitted()['button-edit']).toBe(undefined)

    const button = wrapper.findAll('button').wrappers
    button[2].trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['button-edit'].length).toBe(1)
    expect(wrapper.emitted()['button-edit'][0]).toEqual([
        {
            id: '1',
            name: 'María',
            surname: 'Fernández',
            telephone: '45564356',
            email: 'blabla@gmail.com',
        },
    ])
})

test('Pinta el array', () => {
    const wrapper = shallowMount(ContactDetails, {
        propsData: {
            selectedContact: {
                id: '1',
                name: 'María',
                surname: 'Fernández',
                telephone: '45564356',
                email: 'blabla@gmail.com',
            },
        },
    })
    expect(wrapper.text()).toContain('María')
    expect(wrapper.text()).toContain('Fernández')
    expect(wrapper.text()).toContain('45564356')
    expect(wrapper.text()).toContain('blabla@gmail.com')
})

test('Manda evento para eliminar', async () => {
    const wrapper = shallowMount(ContactDetails, {
        data() {
            return {
                clicked: true,
            }
        },
        propsData: {
            selectedContact: {
                id: '1',
                name: 'María',
                surname: 'Fernández',
                telephone: '45564356',
                email: 'blabla@gmail.com',
            },
        },
    })

    console.log('wrapper', wrapper.html())

    expect(wrapper.emitted()['send-to-delete']).toBe(undefined)

    const deleteContact = wrapper.find(DeleteContact)
    console.log(deleteContact)
    deleteContact.vm.$emit('button-delete-event')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['send-to-delete'].length).toBe(1)
    expect(wrapper.emitted()['send-to-delete'][0]).toEqual([
        {
            id: '1',
            name: 'María',
            surname: 'Fernández',
            telephone: '45564356',
            email: 'blabla@gmail.com',
        },
    ])
})
