import { shallowMount } from '@vue/test-utils'
import ContactAddEdit from '@/components/ContactAddEdit.vue'

test('Pinta los input del formulario en pantalla', () => {
    const wrapper = shallowMount(ContactAddEdit, {
        propsData: {
            selectedContact: { id: null, name: null, surname: null },
        },
    })
    let input = wrapper.find('input')
    expect(input.is('input')).toBe(true)
    let button = wrapper.find('button')
    expect(button.is('button')).toBe(true)
})

test('Pinta los props y el watch funciona', async () => {
    const wrapper = shallowMount(ContactAddEdit, {
        propsData: {
            selectedContact: {
                id: '1',
                name: 'María',
                surname: 'Fernández',
                telephone: null,
                email: null,
            },
        },
    })

    await wrapper.vm.$nextTick()

    expect(wrapper.vm.localContact.name).toBe('María')
    expect(wrapper.vm.localContact.surname).toBe('Fernández')
})

test('Se cambia la prop SelectedContact cuando se pinta nuevo input', async () => {
    const wrapper = shallowMount(ContactAddEdit, {
        propsData: {
            selectedContact: {
                id: '1',
                name: null,
                surname: null,
                telephone: null,
                email: null,
            },
        },
    })

    const nameInput = wrapper.findAll('.name-input').wrappers
    nameInput[0].setValue('María')

    const surnameInput = wrapper.findAll('.surname-input').wrappers
    surnameInput[0].setValue('Fernández')

    const telephoneInput = wrapper.findAll('.telephone-input').wrappers
    telephoneInput[0].setValue('45564356')

    const emailInput = wrapper.findAll('.email-input').wrappers
    emailInput[0].setValue('blabla@gmail.com')

    expect(wrapper.vm.localContact.name).toBe('María')
    expect(wrapper.vm.localContact.surname).toBe('Fernández')
    expect(wrapper.vm.localContact.telephone).toBe('45564356')
    expect(wrapper.vm.localContact.email).toBe('blabla@gmail.com')
})

test('Emite evento del botón back para volver a ContactDetails', async () => {
    const wrapper = shallowMount(ContactAddEdit, {
        propsData: {
            selectedContact: [{ id: null, name: null, surname: null }],
        },
    })
    expect(wrapper.emitted()['emit-back']).toBe(undefined)

    const button1 = wrapper.find('#back-button')

    button1.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['emit-back'].length).toBe(1)
    expect(wrapper.emitted()['emit-back'][0]).toEqual([])
})

test('Emite evento del botón ok que pasa como argumento el contacto modificado/creado', async () => {
    const wrapper = shallowMount(ContactAddEdit, {
        propsData: {
            selectedContact: {
                id: '1',
                name: 'María',
                surname: 'Fernández',
                telephone: '45564356',
                email: 'blabla@gmail.com',
            },
        },
    })
    expect(wrapper.emitted()['emit-back']).toBe(undefined)

    const button1 = wrapper.find('#ok-button')

    button1.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted()['emit-ok'].length).toBe(1)
    expect(wrapper.emitted()['emit-ok'][0]).toEqual([
        {
            id: '1',
            name: 'María',
            surname: 'Fernández',
            telephone: '45564356',
            email: 'blabla@gmail.com',
        },
    ])
})
