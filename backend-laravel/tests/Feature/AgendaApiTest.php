<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

use Illuminate\Support\Facades\DB;

class agendaApiTest extends TestCase
{
    /*
     * La función setUp se ejecuta antes de cada test
     * la utilizamos para generar los datos de prueba
     * en la base de datos de los tests
     */
    protected function setUp(): void
    {
        parent::setUp();
        DB::unprepared("
            CREATE TABLE  agenda (
                id	TEXT NOT NULL,
                name	TEXT,
                surname	TEXT,
                telephone	TEXT,
                email	TEXT
            );
            INSERT INTO agenda VALUES ('1','María', 'Fernández','3248264','mfer@gmail.com');
            INSERT INTO agenda VALUES ('2','nhhik', 'if','32482u7gt64','mfer@gmail.com');

        ");
         //$this->withoutExceptionHandling();
    }

    public function testGetagenda()
    {
        $this->json('GET', 'api/contacts')
            // assertStatus comprueba es status code de la respuesta
            ->assertStatus(200)
            // assertJson comprueba que el objeto JSON que se devuelve es compatible
            // con lo que se espera. 
            // También existe assertExactJson para comprobaciones exactas
            // https://laravel.com/docs/5.8/http-tests#testing-json-apis
            ->assertJson([
                [
                    'id' => '1',
                    'name' => 'María',
                    'surname' => 'Fernández',
                    'telephone' => '3248264',
                    'email' => 'mfer@gmail.com',
                ],
                [
                    'id' => '2',
                    'name' => 'nhhik',
                    'surname' => 'if',
                    'telephone' => '32482u7gt64',
                    'email' => 'mfer@gmail.com',
                ],
            ]);
    }

    public function testGetContact()
    {
        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson(
                [
                    'id' => '1',
                    'name' => 'María',
                    'surname' => 'Fernández',
                    'telephone' => '3248264',
                    'email' => 'mfer@gmail.com',
                ]
            );
    }

    public function testPostContact()
    {
        $this->json('POST', 'api/contacts', [
            'id' => '1',
                    'name' => 'María',
                    'surname' => 'Fernández',
                    'telephone' => '3248264',
                    'email' => 'mfer@gmail.com',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'name' => 'María',
                'surname' => 'Fernández',
                'telephone' => '3248264',
                'email' => 'mfer@gmail.com',
            ]);

        $this->json('GET', '/api/contacts/1')
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                    'name' => 'María',
                    'surname' => 'Fernández',
                    'telephone' => '3248264',
                    'email' => 'mfer@gmail.com',
            ]);
    }

    public function testPut()
    {
        $data = [
            'id' => '1',
                    'name' => 'María',
                    'surname' => 'Fernández',
                    'telephone' => '3248264',
                    'email' => 'mfer@gmail.com',
              
        ];

        $expected = [
            'id' => '1',
                    'name' => 'María',
                    'surname' => 'Fernández',
                    'telephone' => '3248264',
                    'email' => 'mfer@gmail.com',
              
        ];

        $this->json('PUT', 'api/contacts/1', $data)
            ->assertStatus(200)
            ->assertJson($expected);

        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson($expected);
    }

    public function testPatch()
    {
        $this->json('PATCH', 'api/contacts/1', [
            'name' => 'María',
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'name' => 'María',
                'surname' => 'Fernández',
                'telephone' => '3248264',
                'email' => 'mfer@gmail.com',
            ]);

        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson([
                'name' => 'María',
            ]);

        $this->json('PATCH', 'api/contacts/1', [
            'surname' => 'Fernández',
            'telephone' => '3248264',
            'email' => 'mfer@gmail.com',
              
        ])
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                'name' => 'María',
                'surname' => 'Fernández',
                'telephone' => '3248264',
                'email' => 'mfer@gmail.com',
                  
            ]);

        $this->json('GET', 'api/contacts/1')
            ->assertStatus(200)
            ->assertJson([
                'id' => '1',
                    'name' => 'María',
                    'surname' => 'Fernández',
                    'telephone' => '3248264',
                    'email' => 'mfer@gmail.com',
            ]);
    }

    public function testDeleteContact()
    {
        $this->json('GET', 'api/contacts/1')->assertStatus(200);

        $this->json('DELETE', 'api/contacts/1')->assertStatus(200);

        $this->json('GET', 'api/contacts/1')->assertStatus(404);
    }

    public function testGetContactNotExist()
    {
        // $this->json('GET', 'api/contacts/22')->assertStatus(404);

        $this->json('DELETE', 'api/contacts/22')->assertStatus(404);

        $this->json('PUT', 'api/contacts/22', 
            [
                'id' => '1',
                    'name' => 'María',
                    'surname' => 'Fernández',
                    'telephone' => '3248264',
                    'email' => 'mfer@gmail.com',
                  
            ]
        )
            ->assertStatus(404);

        $this->json('PATCH', 'api/contacts/22', [
            'name' => 'María',
        ])
            ->assertStatus(404);
    }
}