# con-tacto

**Una agenda con mucho tacto.**

A contact agenda, it works like your regular phone agenda.
You have a list of contacts, it includes the possibility of adding, editing or removing any contact.
It's responsive.

## Tecnologías / Technologies

Vue
PHP Laravel
Jest

## Autoría / Authors

María Fernández - Imanol Garrido - Joseba Montero

## Standard de código / Code Standard

NAMING
camelCase - Todo lo que no sea de las anteriores
PascalCase - Componentes, (componentes únicos en dos palabras?)
kebab-case - Eventos y tags HTML (component tag) y CSS (class, id)
snake_case - Es para PHP
UPPER_CASE - Variable constante, valor que NUNCA cambia (api key, por ej)


Prettier 4 espacios

=======
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
>>>>>>> 1357b08f6e3bab1e197aa982480c9535fd188cf4
